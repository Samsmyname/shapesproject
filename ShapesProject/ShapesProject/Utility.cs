﻿using System.Collections.Generic;
using System.Xml.Linq;
using System.Drawing;
using System.IO;
using System.Xml;

namespace ShapesProject
{
    class Utility
    {

        public static Shape LoadShape(string name)
        {
            List<Point> pointList = new List<Point>();
            string path = Directory.GetCurrentDirectory() + "\\" + name + ".xml";
            if (File.Exists(path))
            {
                XDocument xmlMap = XDocument.Load(path);

                var points = xmlMap.Element("shape").Elements();
                foreach (var point in points)
                {
                    if (point.Element("x") != null)
                    {
                        Point p = new Point();
                        p.X = int.Parse(point.Element("x").Value);
                        p.Y = int.Parse(point.Element("y").Value);
                        pointList.Add(p);
                    }
                }
                Shape shape;
                var radius = xmlMap.Element("shape").Element("radius");
                if (radius != null)
                {
                    shape = new Circle(pointList, int.Parse(radius.Value));
                }
                else
                {
                    shape = new Shape(pointList);
                }
                return shape;
            }
            else
            {
                return null;
            }

            
        }

        public static void SaveShape(string name, Shape shape)
        {
            string s = "<shape>";

            foreach (Point pt in shape.GetPoints())
            {
                s += "<point><x>" + pt.X + "</x><y>" + pt.Y + "</y></point>";
            }

            if (shape is Circle)
            {
                s += "<radius>" + (shape as Circle).GetRadius() + "</radius>";
            }

            s += "</shape>";

            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(s);
            xdoc.Save(name + ".xml");
        }
    }
}
