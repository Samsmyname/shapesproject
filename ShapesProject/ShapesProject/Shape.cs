﻿using System.Collections.Generic;
using System.Drawing;

namespace ShapesProject
{
    class Shape
    {
        protected List<Point> points;

        public Shape()
        {
            points = new List<Point>();
        }
        public Shape(List<Point> points)
        {
            this.points = points;
        }

        public void AddPoint(Point pt)
        {
            points.Add(pt);
        }

        public virtual void Draw(Graphics l, Pen p)
        {
            if (points.Count >= 3)
            {
                for (int i = 0; i < points.Count; i++)
                {
                    Point pt;
                    Point nextpt;
                    if (i + 1 < points.Count)
                    {
                        pt = points[i];
                        nextpt = points[i + 1];
                    }
                    else
                    {
                        pt = points[i];
                        nextpt = points[0];
                    }

                    l.DrawLine(p, pt.X, pt.Y, nextpt.X, nextpt.Y);
                }
            }
            else if (points.Count >= 2)
            {
                Point pt = points[0];
                Point nextpt = points[1];
                l.DrawLine(p, pt.X, pt.Y, nextpt.X, nextpt.Y);
            }
        }

        public List<Point> GetPoints()
        {
            return points;
        }

    }
}
