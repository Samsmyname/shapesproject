﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace ShapesProject
{
    public partial class Form : System.Windows.Forms.Form
    {
        public Form()
        {
            InitializeComponent();
        }

        private void Form_MouseClick(object sender, MouseEventArgs e)
        {
            //Normal
            if (Program.shapeMode == 1)
            {
                Point pt = new Point();
                pt.X = e.X;
                pt.Y = e.Y;
                Program.ActiveShape.AddPoint(pt);
            }
            //Circle
            else if (Program.shapeMode == 2)
            {
                if (Program.ActiveShape.GetPoints().Count <=0)
                {
                    Point pt = new Point();
                    pt.X = e.X;
                    pt.Y = e.Y;
                    Program.ActiveShape.AddPoint(pt);
                }
                else
                {
                    var r = 
                        Math.Sqrt(
                            Math.Pow((e.X - Program.ActiveShape.GetPoints()[0].X), 2)
                            + Math.Pow((e.Y - Program.ActiveShape.GetPoints()[0].Y), 2)
                        );
                    (Program.ActiveShape as Circle).SetRadius( (int)Math.Floor(r) );
                    Program.shapeMode = 0;
                }
            }

            this.Refresh();

        }

        private void Form_Paint (object sender, PaintEventArgs e)
        {
            Graphics l = e.Graphics;
            Pen p = new Pen(colorDialog1.Color, 5);
            Program.ActiveShape.Draw(l ,p);
            l.Dispose();
        }

        //Save Shape Button
        private void button1_Click(object sender, EventArgs e)
        {
            Program.shapeMode = 0;
            label2.Text = "Saved Shape!";
            label2.ForeColor = Color.Green;
            Utility.SaveShape(textBox1.Text, Program.ActiveShape);
        }

        //Load Shape Button
        private void button2_Click(object sender, EventArgs e)
        {
            Program.shapeMode = 0;
            label2.Text = "";
            label2.ForeColor = Color.Red;
            if (this.textBox1.Text != "")
            {
                Shape s = Utility.LoadShape(this.textBox1.Text);
                if (s != null)
                {
                    Program.ActiveShape = s;
                    label2.ForeColor = Color.Green;
                    label2.Text = "Shape Loaded!";
                }
                else
                {
                    label2.Text = "File not found!";
                }
            }
            else
                label2.Text = "Invalid file name!";

            this.Refresh();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        //Create Shape
        private void button3_Click(object sender, EventArgs e)
        {
            Program.ActiveShape = new Shape();
            Program.shapeMode = 1;
            this.Refresh();
        }

        //Create Circle
        private void button4_Click(object sender, EventArgs e)
        {
            Program.ActiveShape = new Circle();
            Program.shapeMode = 2;
            this.Refresh();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (this.colorDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.BackColor = colorDialog1.Color;
                this.Refresh();
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
