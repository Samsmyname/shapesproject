﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Drawing;

namespace ShapesProject
{
    static class Program
    {
        public static Shape ActiveShape;
        public static int shapeMode;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ActiveShape = new Shape();
            shapeMode = 0;
            
            List<Point> testPoints = new List<Point>();
            testPoints.Add(new Point(100, 100));
            testPoints.Add(new Point(100, 200));
            testPoints.Add(new Point(200, 200));
            ActiveShape = new Shape(testPoints);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form());


        }
    }
}
