﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapesProject
{
    class Circle : Shape
    {
        private int radius = 0;

        public Circle(List<Point> points, int r) : base(points)
        {
            this.points = points;
            radius = r;
        }

        public Circle()
        {
        }

        public void SetRadius(int r)
        {
            radius = r;

        }

        public int GetRadius()
        {
            return radius;
        }

        public override void Draw(Graphics l, Pen p)
        {
            if (Program.ActiveShape.GetPoints().Count >= 1 && (Program.ActiveShape as Circle).radius > 0)
                l.DrawEllipse(p, 
                    Program.ActiveShape.GetPoints()[0].X - (radius/2), 
                    Program.ActiveShape.GetPoints()[0].Y - (radius/2), 
                    radius, radius);

        }

    }
}
